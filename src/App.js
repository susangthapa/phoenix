import Header from './views/layouts/Header';
import Footer from './views/layouts/Footer';
import Home from './views/Home';
import Aside from './views/layouts/Aside';
import * as routes from './router/routes';
import history from './router/history';
import { Route, Router, Switch } from 'react-router-dom';
import News from './views/News';
import Notices from './views/Notices';

function App() {
  return (
    <Router history={history}>
      <Header />
      <main role="main" className="container p-t-10" >
        <div className="row">
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path={routes.newsPath} exact component={News} />
            <Route path={routes.noticesPath} exact component={Notices} />
          </Switch>

          <Aside />
        </div>
      </main>
      <Footer />
    </Router>
  );
}

export default App;
