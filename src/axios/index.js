import axios from 'axios';

const api = axios.create({
    baseURL: process.env.REACT_APP_API_ROOT + '/api',
    headers: {
        Accept: 'application/json'
    }
});

export default api;