export const newsPath = '/news';
export const singleNewsPath = '/news/:newsSlug';

export const noticesPath = '/notices';
export const singleNoticePath = '/notices/:noticeSlug';
